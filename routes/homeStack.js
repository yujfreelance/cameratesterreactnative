import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import Home from '../screens/home';
import About from '../screens/about';
import Diagnostic from '../screens/diagnostic';
import CamResult from '../screens/tests/camResult';
import CamTester from '../screens/tests/camTester';
import DiagnosticResult from '../screens/tests/diagnosticResult';

const screens = {
    HomeScreen: {
        screen: Home,
        navigationOptions: {
            headerShown: false,
            // header: null,
        },
    }
    ,CamResultScreen:{
        screen: CamResult,
        navigationOptions: {
            headerShown: false,
            // header: null,
        },
    }
    ,CamTesterScreen:{
        screen: CamTester,
        navigationOptions: {
            headerShown: false,
            // header: null,
        },
    }
    ,DiagnosticResultScreen:{
        screen: DiagnosticResult,
        navigationOptions: {
            headerShown: false,
            // header: null,
        },
    }
}
const HomeStack = createStackNavigator(screens);

export default createAppContainer(HomeStack);