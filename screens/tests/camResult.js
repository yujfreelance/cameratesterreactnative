import React from 'react';
import {TEST_TYPE } from '../home';

import { StyleSheet, Text, View,Image,
    TextInput,
    Button,ImageBackground,
    StatusBar,
    SafeAreaView,
    TouchableOpacity} from 'react-native';
    
import { Camera } from "expo-camera";
import { Video,AVPlaybackStatus } from "expo-av";
export default function CamResult({navigation}){

    const video = React.useRef(null);
    const [status, setStatus] = React.useState({});
    const passTestHandler = () => {
        let prevType=navigation.getParam('type');
        let diagnostic=navigation.getParam('diagnostic');
        let onRetry=navigation.getParam('onRetry');
        switch (prevType.mode) {
            case TEST_TYPE.PHOTO_REAR.mode:
                diagnostic.photo_rear=true;
                if(onRetry){
                    navigation.push('DiagnosticResultScreen',
                    {
                        diagnostic:diagnostic
                    });
                }else{
                    navigation.push('CamTesterScreen',
                    {
                        type:TEST_TYPE.VIDEO_REAR,
                        diagnostic:diagnostic
                    });
                }
                break;
            case TEST_TYPE.VIDEO_REAR.mode:
                video.current.pauseAsync();
                diagnostic.video_rear=true;
                if(onRetry){
                    navigation.push('DiagnosticResultScreen',
                    {
                        diagnostic:diagnostic
                    });
                }else{
                navigation.push('CamTesterScreen',
                {
                    type:TEST_TYPE.PHOTO_FRONT,
                    diagnostic:diagnostic
                });
                }
                break;
            case TEST_TYPE.PHOTO_FRONT.mode:
                diagnostic.photo_front=true;
                if(onRetry){
                    navigation.push('DiagnosticResultScreen',
                    {
                        diagnostic:diagnostic
                    });
                }else{
                navigation.push('CamTesterScreen',
                {
                    type:TEST_TYPE.VIDEO_FRONT,
                    diagnostic:diagnostic
                });
                }
                break;
            case TEST_TYPE.VIDEO_FRONT.mode:
                video.current.pauseAsync();
                diagnostic.video_front=true;
                navigation.navigate('DiagnosticResultScreen',
                {
                    diagnostic:diagnostic
                });
                break;
            default:return null;
        }
        console.log('updated diagnostic',diagnostic);
        
    }

    const retryTestHandler = () => {
        let prevType=navigation.getParam('type');
        let diagnostic = navigation.getParam('diagnostic');
        let onRetry = navigation.getParam('onRetry'); 


        switch (prevType.mode) {
            case TEST_TYPE.VIDEO_REAR.mode:
                video.current.pauseAsync();
            break;
            case TEST_TYPE.VIDEO_FRONT.mode:
                video.current.pauseAsync();
            break;
            }

        navigation.push('CamTesterScreen',
            {
            type:prevType,
            diagnostic:diagnostic,
            onRetry:onRetry
            
        });
    }
    const getResultLabel=()=>{
        let testType=navigation.getParam('type').mode;
        switch (testType) {
            case TEST_TYPE.PHOTO_REAR.mode: return 'Rear Camera Photo'; break;
            case TEST_TYPE.VIDEO_REAR.mode: return 'Rear Camera Video'; break;
            case TEST_TYPE.PHOTO_FRONT.mode: return 'Front Camera Photo'; break;
            case TEST_TYPE.VIDEO_FRONT.mode: return 'Front Camera Video'; break;
        break;
        }
    }
    const failTestHandler = () => {
        let prevType=navigation.getParam('type');
        let diagnostic=navigation.getParam('diagnostic');
        let onRetry=navigation.getParam('onRetry');

        switch (prevType.mode) {
            case TEST_TYPE.PHOTO_REAR.mode:
                diagnostic.photo_rear=false;
                if(onRetry){
                    navigation.push('DiagnosticResultScreen',
                    {
                        diagnostic:diagnostic
                    });
                }else{
                    navigation.push('CamTesterScreen',
                    {
                        type:TEST_TYPE.VIDEO_REAR,
                        diagnostic:diagnostic
                    });
                }
                break;
            case TEST_TYPE.VIDEO_REAR.mode:
                video.current.pauseAsync();
                diagnostic.video_rear=false;

                if(onRetry){
                    navigation.push('DiagnosticResultScreen',
                    {
                        diagnostic:diagnostic
                    });
                }else{
                    navigation.push('CamTesterScreen',
                    {
                        type:TEST_TYPE.PHOTO_FRONT,
                        diagnostic:diagnostic
                    });
                 }
                break;
            case TEST_TYPE.PHOTO_FRONT.mode:
                diagnostic.photo_front=false;

                if(onRetry){
                    navigation.push('DiagnosticResultScreen',
                    {
                        diagnostic:diagnostic
                    });
                }else{
                    navigation.push('CamTesterScreen',
                    {
                        type:TEST_TYPE.VIDEO_FRONT,
                        diagnostic:diagnostic
                    });
                }
                break;
            case TEST_TYPE.VIDEO_FRONT.mode:
                video.current.pauseAsync();
                diagnostic.video_front=false;
                navigation.navigate('DiagnosticResultScreen',
                {
                    diagnostic:diagnostic
                });
                break;
            default:return null;
        }
        console.log('updated diagnostic',diagnostic);
    }

   const renderPreview=()=>{
    let testType=navigation.getParam('type').mode;
    switch (testType) {
        case TEST_TYPE.PHOTO_REAR.mode:
        case TEST_TYPE.PHOTO_FRONT.mode:
            return <ImageBackground source={{ uri:navigation.getParam('source') }} style={styles.imageBackground}/>
         break;
        case TEST_TYPE.VIDEO_REAR.mode: 
        case TEST_TYPE.VIDEO_FRONT.mode: 
        return <Video
                ref={video}
      shouldPlay={true}
                style={styles.video}
                source={{
                uri: navigation.getParam('source'),
                }}
                useNativeControls
                resizeMode="contain"
                isLooping
                onPlaybackStatusUpdate={status => setStatus(() => status)}
            />
         break;
    break;
    }
        return ;
    }

    return (
     <View style={styles.container}>
        <View style={{paddingTop:30}}/>

        <Text style={{fontSize:18,fontWeight:'bold',paddingTop:10}}>{getResultLabel()}</Text>
        <Text>{navigation.getParam('mode')}</Text>
        <View style={{flex: 0,width:200,height:360}}>
       {renderPreview()}
        </View>

        <TouchableOpacity style={styles.passBtn} onPress={passTestHandler}>
            <Text style={styles.btnText}>Pass</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.retryBtn} onPress={retryTestHandler}>
            <Text style={styles.btnText}>Retry</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.failBtn} onPress={failTestHandler}>
            <Text style={styles.btnText}>Fail</Text>
        </TouchableOpacity>
    </View>
    )
    ;
}
const styles = StyleSheet.create({
  container: {
    flex: 0,
    backgroundColor: 'rgb(240,247,252)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  passBtn:{
   width:"90%",
   borderRadius:12,
   height:54,
   alignItems:"center",
   justifyContent:"center",
   marginTop:42,
   backgroundColor:"rgb(52,180,121)"
 },
 retryBtn:{
  width:"86%",
  borderRadius:12,
  height:54,
  alignItems:"center",
  justifyContent:"center",
  marginTop:12,
  backgroundColor:"rgb(0,163,204)"
},
failBtn:{
 width:"86%",
 borderRadius:12,
 height:54,
 alignItems:"center",
 justifyContent:"center",
 marginTop:12,
 backgroundColor:"rgb(232,94,69)"
},
 btnText:{
   fontFamily: 'sans-serif-light',
   fontWeight: 'bold',
   color:'white',
   fontSize:18
 },
 imageBackground: {
    flex: 1,
    resizeMode: 'cover',
    width: '100%',
    alignItems: 'center'
  },video: {
    alignSelf: 'center',
    width: 360,
    height: 360,
  }
});