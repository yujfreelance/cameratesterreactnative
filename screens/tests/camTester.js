import React, { useState, useRef, useEffect } from "react";
import {
  StyleSheet,
  Dimensions,
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import { Camera } from "expo-camera";
import { Video } from "expo-av";
import {TEST_TYPE } from '../home';
import Icon from 'react-native-vector-icons/AntDesign';
import IconEnty from 'react-native-vector-icons/Entypo';
const WINDOW_HEIGHT = Dimensions.get("window").height;

const closeButtonSize = Math.floor(WINDOW_HEIGHT * 0.032);
const captureSize = Math.floor(WINDOW_HEIGHT * 0.09);

export default function CamTester({navigation}) {
  const [hasPermission, setHasPermission] = useState(null);
  const [cameraType, setCameraType] = useState(Camera.Constants.Type.back);
  const [isPreview, setIsPreview] = useState(false);
  const [isCameraReady, setIsCameraReady] = useState(false);
  const [isVideoRecording, setIsVideoRecording] = useState(false);
  const [captureSource, setCaptureSource] = useState(null);
  const cameraRef = useRef();

  useEffect(() => {
    (async () => {
        const { status } = await Camera.requestCameraPermissionsAsync();
        setHasPermission(status === "granted");
        const { status2 } = await Camera.requestMicrophonePermissionsAsync();
    })();
  }, []);

  const onCameraReady = () => {
    setIsCameraReady(true);
    let testType = navigation.getParam('type');
    switch (testType.mode) {
      case TEST_TYPE.PHOTO_FRONT.mode: setCameraType(Camera.Constants.Type.front); break;
      case TEST_TYPE.VIDEO_FRONT.mode: setCameraType(Camera.Constants.Type.front); break;
      case TEST_TYPE.PHOTO_REAR.mode:  setCameraType(Camera.Constants.Type.back); break;
      case TEST_TYPE.VIDEO_REAR.mode:  setCameraType(Camera.Constants.Type.back); break;
    }
    setIsPreview(false);
    
  };

  const takePicture = async () => {
    if (cameraRef.current) {
      const options = { quality: 0.5, base64: true, skipProcessing: true };
      const data = await cameraRef.current.takePictureAsync(options);
      const source = data.uri;
      if (source) {
        await cameraRef.current.pausePreview();
        setIsPreview(true);
        setCaptureSource(source);
        console.log("picture source", source);
      }
    }
  };

  const recordVideo = async () => {
    if (cameraRef.current) {
      try {
        const videoRecordPromise = cameraRef.current.recordAsync();

        if (videoRecordPromise) {
          setIsVideoRecording(true);
          const data = await videoRecordPromise;
          const source = data.uri;
          if (source) {
            setIsPreview(true);
            console.log("video source", source);
            setCaptureSource(source);
          }
        }
      } catch (error) {
        console.warn(error);
      }
    }
  };

  const stopVideoRecording = () => {
    if (cameraRef.current) {
      setIsPreview(false);
      setIsVideoRecording(false);
      cameraRef.current.stopRecording();
  
      // acceptCapture();
    }
  };

  const switchCamera = () => {
    if (isPreview) {
      return;
    }
    setCameraType((prevCameraType) =>
      prevCameraType === Camera.Constants.Type.back
        ? Camera.Constants.Type.front
        : Camera.Constants.Type.back
    );
  };

  const cancelPreview = async () => {
    await cameraRef.current.resumePreview();
    setIsPreview(false);
    setCaptureSource(null);
  };

  const renderCancelPreviewButton = () => (
    <TouchableOpacity onPress={cancelPreview} style={styles.closeButton}>
      <View style={[styles.closeCross, { transform: [{ rotate: "45deg" }] }]} />
      <View
        style={[styles.closeCross, { transform: [{ rotate: "-45deg" }] }]}
      />
    </TouchableOpacity>
  );

  const renderAcceptCaptureBtn = () =>(
    <View style={styles.control}>
      <TouchableOpacity
        activeOpacity={0.7}
        onPress={acceptCapture}
        style={styles.acceptCapture}
        >
         <Text style={{paddingTop:15,paddingLeft:14, flex:0, alignItems:'center'}}> <Icon name="doubleright" size={30} color="white" /></Text>
      </TouchableOpacity>
    </View>
  );

  const renderVideoPlayer = () => (
    <Video
      source={{ uri: captureSource }}
      shouldPlay={true}
      style={styles.media}
    />
  );

  const renderVideoRecordIndicator = () => (
    <View style={styles.recordIndicatorContainer}>
      <View style={styles.recordDot} />
      <Text style={styles.recordTitle}>{"Recording..."}</Text>
    </View>
  );


  renderCamBtn = () => {
    let testType = navigation.getParam('type');
    switch (testType.mode) {
      case TEST_TYPE.PHOTO_FRONT.mode: return renderCaptureBtn(); break;
      case TEST_TYPE.VIDEO_FRONT.mode: return renderVideoRecBtn(); break;
      case TEST_TYPE.PHOTO_REAR.mode:  return renderCaptureBtn(); break;
      case TEST_TYPE.VIDEO_REAR.mode:  return renderVideoRecBtn(); break;
      default:return null;
    }
  };

  const renderCaptureBtn = () => (
    <View style={styles.control}>
      <TouchableOpacity
        activeOpacity={0.7}
        disabled={!isCameraReady}
        onPress={takePicture}
        style={styles.capture}
      >
        <Text style={{paddingLeft:16,paddingTop:15, flex:0, alignItems:'center'}}><Icon name="camera" size={32} color="rgba(13,102,194,0.8)" /></Text>
      </TouchableOpacity>
    </View>
  );

  
  const acceptCapture = () => {
    let diagnostic = navigation.getParam('diagnostic');
    let onRetry = navigation.getParam('onRetry');
    navigation.navigate('CamResultScreen',
    {
      source:captureSource,
      type:navigation.getParam('type'),
      onRetry:onRetry,
      diagnostic:diagnostic
    }
    );
  }

  const recordVideoToggle = async () => {
    if(isVideoRecording){
      stopVideoRecording();
    }else{
      recordVideo();
    }
  }

  const shouldShowCam=()=>{
    
    let testType = navigation.getParam('type');
    let isVideo=testType.mode==TEST_TYPE.VIDEO_REAR.mode || testType.mode==TEST_TYPE.VIDEO_FRONT.mode;
    if(isVideo && captureSource ){
      console.log('vid ito, may narecord na');
      return false;
    }
    console.log('captureSource',captureSource);
    console.log('testType.mode',testType.mode);
    return true;
  }

  const renderVideoRecBtn = () => (
    <View style={styles.control}>
      <TouchableOpacity
        activeOpacity={0.7}
        disabled={!isCameraReady}
        onPress={recordVideoToggle}
        style={styles.capture}
      >
        {!isVideoRecording?
        <Text style={{paddingLeft:16,paddingTop:15, flex:0, alignItems:'center'}}><IconEnty name="controller-record" size={32} color="rgba(13,102,194,0.8)" /></Text>:
        <Text style={{paddingLeft:16,paddingTop:15, flex:0, alignItems:'center'}}><IconEnty name="controller-stop" size={32} color="rgba(13,102,194,0.8)" /></Text>}
      </TouchableOpacity>
    </View>
  );

  if (hasPermission === null) {
    return <View />;
  }

  if (hasPermission === false) {
    return <Text style={styles.text}>No access to camera</Text>;
  }

  

  return (
    <SafeAreaView style={styles.container}>
     {shouldShowCam() &&
      <Camera
        ref={cameraRef}
        style={styles.container}
        type={cameraType}
        flashMode={Camera.Constants.FlashMode.off}
        onCameraReady={onCameraReady}
        onMountError={(error) => {
          console.log("cammera error", error);
        }}
      />}
      <View style={styles.container}>
      <Text>{ navigation.getParam('mode')}</Text>
        {isVideoRecording && renderVideoRecordIndicator()}
        {captureSource && renderVideoPlayer()}
        {/* {isPreview && renderCancelPreviewButton()} */}
        {!captureSource && !isPreview && renderCamBtn()}
        {isPreview && renderAcceptCaptureBtn()}

      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
  },
  closeButton: {
    position: "absolute",
    top: 35,
    left: 15,
    height: closeButtonSize,
    width: closeButtonSize,
    borderRadius: Math.floor(closeButtonSize / 2),
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#c4c5c4",
    opacity: 0.7,
    zIndex: 2,
  },
  media: {
    ...StyleSheet.absoluteFillObject,
    resizeMode:Video.RESIZE_MODE_STRETCH
  },
  closeCross: {
    width: "68%",
    height: 1,
    backgroundColor: "black",
  },
  control: {
    position: "absolute",
    flexDirection: "row",
    bottom: 38,
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  acceptCapture: {
    backgroundColor: "rgba(0,0,0,0.2)",
    borderRadius: 5,
    height: captureSize,
    width: captureSize,
    borderRadius: Math.floor(captureSize / 2),
    marginHorizontal: 31,
  },
  capture: {
    backgroundColor: "#f5f6f5",
    borderRadius: 5,
    height: captureSize,
    width: captureSize,
    borderRadius: Math.floor(captureSize / 2),
    marginHorizontal: 31,
  },
  recordIndicatorContainer: {
    flexDirection: "row",
    position: "absolute",
    top: 25,
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    opacity: 0.7,
  },
  recordTitle: {
    fontSize: 14,
    color: "#ffffff",
    textAlign: "center",
  },
  recordDot: {
    borderRadius: 3,
    height: 6,
    width: 6,
    backgroundColor: "#ff0000",
    marginHorizontal: 5,
  },
  text: {
    color: "#fff",
  },
});