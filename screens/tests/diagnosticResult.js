import React from 'react';

import { StyleSheet, Text, View,Image,
    TextInput,
    Button,
    StatusBar,
    ImageBackground,
    ToastAndroid,
    TouchableOpacity} from 'react-native';

  import * as Device from 'expo-device';
  import {TEST_TYPE } from '../home';
  import * as Print from 'expo-print';
  import { shareAsync } from 'expo-sharing';
  export default function DiagnosticResult({navigation}){

  const printToFile = async (zz) => {
    // On iOS/android prints the given html. On web prints the HTML from the current page. 
    const { uri } = await Print.printToFileAsync({html:getHTMLRaw()});
    console.log('File has been saved to:', uri);
    await shareAsync(uri, { UTI: '.pdf', mimeType: 'application/pdf' });
  }

  const getHTMLRaw = () => {
    let diagnostic = navigation.getParam('diagnostic');

    let dateTime = new Date().toString(); 
    let deviceName = Device.deviceName;
    let deviceModel = Device.modelName;

    let raw= `<html>
    <head>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
    </head>
    <body style="text-align: center;">
      <h1 style="font-size: 50px; font-family: Helvetica Neue; font-weight: normal;">
        Diagnostics Results
      </h1>`

      +'<br/> <h2>'+ 'Rear Camera Photo:' +'</h2>'
      +'<h1>' +(diagnostic.photo_rear?'PASSED':'FAILED') + '</h1>'

      +'<br/> <h2>'+ 'Rear Camera Video:' +'</h2>'
      +'<h1>' +(diagnostic.video_rear?'PASSED':'FAILED') + '</h1>'

      +'<br/> <h2>'+ 'Front Camera Photo:' +'</h2>'
      +'<h1>' +(diagnostic.photo_front?'PASSED':'FAILED') + '</h1>'

      +'<br/> <h2>'+ 'Front Camera Video:' +'</h2>'
      +'<h1>' +(diagnostic.video_front?'PASSED':'FAILED') + '</h1>'

      +'<div style="text-align: left;"> <br/> '
      +'<h2 style="border-bottom: 0px">Device name: ' + deviceName  +' </h2>'
      +'<h2>Device model: ' + deviceModel  +' </h2>'
      +'<h2>Date/Time: ' + dateTime  +' </h2> </div>'

      +'<br/>  </body> </html>';
      console.log('htmlraw',raw);
      return raw;
      }
 
   const retestBtnHandler = (testType) => {
        let diagnostic = navigation.getParam('diagnostic');
        navigation.push('CamTesterScreen',
        {
          type:testType,
          diagnostic:diagnostic,
          onRetry:true
        });
    }

    const submitDiagnostic = () => {
      printToFile('');
      navigation.navigate('HomeScreen');
   }

    const renderTestResult=(label, testType)=>{
      let diagnostic = navigation.getParam('diagnostic');

      let canRetest = false;
      switch (testType.mode) {
        case TEST_TYPE.PHOTO_FRONT.mode: canRetest=!diagnostic.photo_front; break;
        case TEST_TYPE.VIDEO_FRONT.mode: canRetest=!diagnostic.video_front; break;
        case TEST_TYPE.PHOTO_REAR.mode:  canRetest=!diagnostic.photo_rear; break;
        case TEST_TYPE.VIDEO_REAR.mode:  canRetest=!diagnostic.video_rear; break;
        default:return null;
      }

      return <View style={styles.testResult}>
        <View style={{width:'77%'}}><Text style={{fontSize:18,color:'rgba(0,0,0,0.9)'}}>{label}</Text></View>
        <View style={{width:'23%'}}>
            <TouchableOpacity style={canRetest?styles.testResultBtn:styles.testResultBtnInactive}
                onPress={()=>{if(canRetest)retestBtnHandler(testType);}}>
                <Text style={styles.btnText}>Retest</Text>
            </TouchableOpacity>
        </View>
      </View>
    };

    const getPassed=()=>{
      let diagnostic=navigation.getParam('diagnostic');
      let passed=0;
      if(diagnostic.photo_rear)passed++;
      if(diagnostic.video_rear)passed++;
      if(diagnostic.photo_front)passed++;
      if(diagnostic.video_front)passed++;
      return passed;
    };

    return (
     <View style={styles.container}>
        <View style={{paddingTop:34}}/>
        <Text style={styles.diagTop}>Diagnostics Results</Text>

        <View style={{paddingTop:5,paddingBottom:5,paddingLeft:5,paddingRight:5,width:'100%',backgroundColor:'rgba(13,102,194,0.2)'}}>
          <Text style={{width:'100%',fontSize:18,fontWeight:'bold'}}>{'Passed: '+getPassed()}</Text>
        </View>
        {renderTestResult('Rear Camera Photo',TEST_TYPE.PHOTO_REAR)}
        {renderTestResult('Rear Camera Video',TEST_TYPE.VIDEO_REAR)}
        {renderTestResult('Front Camera Photo',TEST_TYPE.PHOTO_FRONT)}
        {renderTestResult('Front Camera Vieo',TEST_TYPE.VIDEO_FRONT)}
  
        <View style={{width:'100%',paddingLeft:10,paddingRight:10}}>
          <TouchableOpacity style={styles.submitBtn} onPress={submitDiagnostic}>
              <Text style={styles.btnText}>Submit</Text>
          </TouchableOpacity>
        </View>
       
    </View>
    )
    ;
}
const styles = StyleSheet.create({
  container: {
    flex: 0,
    backgroundColor: 'rgb(240,247,252)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  
  testResult: {
    flex: 0,
    // backgroundColor: 'rgba(20,20,20,0.32)',
    borderBottomColor:'rgba(0,0,0,0.1)', borderBottomWidth:1,
    // borderTopColor:'rgba(0,0,0,0.1)', borderTopWidth:1,
    alignItems: 'center',
    justifyContent: 'center',

    flexDirection: "row",
    flexWrap: "wrap",
    width:"100%",
    paddingLeft:20,
    paddingRight:20,
    paddingTop:20,
    paddingBottom:20
  },
  testResultBtn:{
   borderRadius:12,
   height:54,
   alignItems:"center",
   justifyContent:"center",
   backgroundColor:"rgb(232,94,69)"
  },
  testResultBtnInactive:{
   borderRadius:12,
   height:54,
   alignItems:"center",
   justifyContent:"center",
   backgroundColor:"rgba(0,0,0,0.2)"
  },
  diagTop:{
    paddingBottom:10,
    fontSize:22,
    fontWeight:'bold',
    paddingTop:15,
    backgroundColor:'rgb(13,102,194)',
    width:'100%',
    textAlign:'center',
    color:'white'
  },



  passBtn:{
   width:"90%",
   borderRadius:12,
   height:54,
   alignItems:"center",
   justifyContent:"center",
   marginTop:42,
   backgroundColor:"rgb(52,180,121)"
 },
 retryBtn:{
  width:"86%",
  borderRadius:12,
  height:54,
  alignItems:"center",
  justifyContent:"center",
  marginTop:12,
  backgroundColor:"rgb(0,163,204)"
},
submitBtn:{
 width:"100%",
 borderRadius:12,
 height:54,
 alignItems:"center",
 justifyContent:"center",
 marginTop:12,
 backgroundColor:"rgb(51,181,121)"
},
 btnText:{
   fontFamily: 'sans-serif-light',
   fontWeight: 'bold',
   color:'white',
   fontSize:18
 },
 imageBackground: {
    flex: 1,
    resizeMode: 'cover',
    width: '100%',
    alignItems: 'center'
  }
});