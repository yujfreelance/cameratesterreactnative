import React from 'react';

import { StyleSheet, Text, View,Image,
    TextInput,
    Button,
    TouchableOpacity} from 'react-native';
    
export default function Diagnostic({navigation}){

  const goToHome = () => {
    // navigation.navigate('HomeScreen')
    navigation.goBack()
}



    return (
     <View style={styles.container}>
     <Text>Diagnostic</Text>

     <TouchableOpacity onPress={goToHome}>
        <Text>Go to home</Text>
      </TouchableOpacity>
    </View>
    )
    ;
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(240,247,252)',
    alignItems: 'center',
    justifyContent: 'center',
  }
});