import React from 'react';
import { BackHandler } from 'react-native';
import { StatusBar } from 'expo-status-bar';

export const TEST_TYPE= Object.freeze({
   PHOTO_REAR:{mode:'PHOTO_REAR'}
  ,VIDEO_REAR:{mode:'VIDEO_REAR'}
  ,VIDEO_FRONT:{mode:'VIDEO_FRONT'}
  ,PHOTO_FRONT:{mode:'PHOTO_FRONT'}
});

import { StyleSheet, Text, View,Image,
    TextInput,
    Button,
    TouchableOpacity} from 'react-native';
    
export default function Home({ navigation }){

    const beginDiagnosticHandler = () => {
        navigation.push('CamTesterScreen',
        {
          type:TEST_TYPE.PHOTO_REAR,
          diagnostic:{
            photo_rear:null,
            video_rear:null,
            photo_front:null,
            video_front:null
          },
          onRetry:false
        });
    }

    const exitBtnHandler = () => {
        BackHandler.exitApp();
    }
    
    const testDiagnosticScreen = () => {
      navigation.navigate('DiagnosticResultScreen',
      {
        diagnostic:{
          photo_rear:false,
          video_rear:true,
          photo_front:false,
          video_front:false
        }
      }
      );
  }

    return (
     <View style={styles.container}>
      {/* <StatusBar style="auto" /> */}
      <Text style={styles.appTitleTxt}>Camera Tester</Text>

      <TouchableOpacity style={styles.beginBtn} onPress={beginDiagnosticHandler}>
        <Text style={styles.beginBtnText}>Begin Diagnostic</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menuSubBtn} onPress={testDiagnosticScreen}>
        <Text style={styles.beginBtnText}>Exit</Text>
      </TouchableOpacity>
    </View>
    )
    ;
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(240,247,252)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  appTitleTxt:{
    color:'black',
    fontFamily: 'sans-serif-light',
    fontSize:48
  },
  beginBtn:{
   width:"84%",
   borderRadius:40,
   height:80,
   alignItems:"center",
   justifyContent:"center",
   marginTop:80,
   backgroundColor:"rgb(60,68,130)"
 },
 menuSubBtn:{
  width:"74%",
  borderRadius:30,
  height:60,
  alignItems:"center",
  justifyContent:"center",
  marginTop:18,
  backgroundColor:"rgb(60,68,130)"
},
  beginBtnText:{
    fontFamily: 'sans-serif-light',
    fontWeight: 'normal',
    color:'white',
    fontSize:20
  }
});